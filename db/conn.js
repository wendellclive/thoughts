const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('thoughts2', 'root', 'senha_da_nasa', {
  host: 'localhost',
  dialect: 'mysql',
});

try {
  sequelize.authenticate();
  console.log('Conectamos com Sucesso!!');
} catch (error) {
  console.log(`Não foi possível conectar: ${error}`);
}

module.exports = sequelize